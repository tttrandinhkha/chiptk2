#Extracting and Transform data
from multiprocessing.sharedctypes import Value
from typing import Type
import pandas as pd
import datetime
import time

def check_range(x,min,max):
    try:
        if x >= min and x<= max:
            return x
        else:
            raise AssertionError
    except AssertionError:
        print('Not IN RANGE')
    except TypeError:
        print('TYPE ERROR: ',TypeError)
        

def load_location(flatten_json,return_list = False):
    try:
        if 'id' in flatten_json.keys():
            df = pd.DataFrame([flatten_json],columns = ['id','name','coord_lon','coord_lat','timezone','sys_country','valid_from','update_at'])
            df['valid_from'] = convert_time(time.time())
            if return_list == True:
                return df.iloc[0].values.tolist()[:-2]
            else:
                return df
        else:
            raise AssertionError
    except AssertionError:
        print("DO NOT EXIST ID")

def load_weather(nested_json,return_list = False):
    df = pd.json_normalize(nested_json,record_path='weather')
    try:
        if df.dtypes.id == 'int64':
            df['valid_from'] = convert_time(time.time())
            df['update_at'] = None
            if return_list and df.shape[0] ==1:
                return(df.iloc[0].values.tolist()[:-2])
            else:
                return df
        else:
            raise AssertionError
    except AssertionError:
        print('ERROR FROM', AssertionError)


def convert_time(time):
    try:
        return datetime.datetime.utcfromtimestamp(time).strftime('%Y-%m-%d %H:%M:%S')
    except TypeError:
        print('Error in coverting time: ',TypeError)


def load_fact(flatten_json):
    features = ['main_temp', 'main_feels_like', 'main_temp_max', 'main_temp_min', 'main_pressure', 'main_humidity','main_sea_level','main_grnd_level',
                'visibility', 'wind_speed', 'wind_deg', 'wind_gust', 'clouds_all','rain_1h','rain_3h','snow_1h','snow_3h',
                'dt','base','sys_sunrise','sys_sunset']

    df = pd.DataFrame([flatten_json],columns = features)
    timezone = flatten_json['timezone']
    df_features = ['sys_sunrise', 'sys_sunset', 'dt']
    for i in df_features:
        local_time = df[i] + timezone
        df[i] = convert_time(local_time)
    return df

