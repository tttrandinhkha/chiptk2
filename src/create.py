create_location = """IF( EXISTS(SELECT * 
                                    FROM INFORMATION_SCHEMA.TABLES
                                    WHERE TABLE_SCHEMA = 'dbo'
                                    AND TABLE_NAME = 'location'))
                            BEGIN
                                PRINT 'TABLE EXISTS'
                            END;
                        ELSE
                            BEGIN
                                CREATE table location(
                                id INT Primary Key,
                                name varchar(40),
                                coord_lon float,
                                coord_lat float,
                                timezone char(10),
                                sys_country char(2),
                                valid_from datetime,
                                update_at datetime)
                            END;"""
#timezone -> float
create_fact_table = """IF( EXISTS(SELECT * 
                                    FROM INFORMATION_SCHEMA.TABLES
                                    WHERE TABLE_SCHEMA = 'dbo'
                                    AND TABLE_NAME = 'fact'))
                            BEGIN
                                PRINT 'TABLE EXISTS'
                            END;
                        ELSE
                            BEGIN
                                CREATE TABLE fact(
                                city_id INT,
                                weather_id INT,
                                main_temp FLOAT,
                                main_feels_like FLOAT,
                                main_temp_max FLOAT,
                                main_temp_min FLOAT,
                                main_pressure INT,
                                main_humidity INT,
                                main_sea_level INT,
                                main_grnd_level INT,
                                visibility FLOAT,
                                wind_speed FLOAT,
                                wind_deg INT,
                                wind_gust FLOAT,
                                clouds_all FLOAT,
                                rain_1h FLOAT,
                                rain_3h FLOAT,
                                snow_1h FLOAT,
                                snow_3h FLOAT,
                                dt DATETIME,
                                base VARCHAR(50),
                                sys_sunrise DATETIME,
                                sys_sunset DATETIME,
                                PRIMARY KEY(weather_id,city_id,dt)
                                )
                            END;"""

create_weather = """IF( EXISTS(SELECT * 
                                    FROM INFORMATION_SCHEMA.TABLES
                                    WHERE TABLE_SCHEMA = 'dbo'
                                    AND TABLE_NAME = 'weather'))
                            BEGIN
                                PRINT 'TABLE EXISTS'
                            END;
                        ELSE
                            BEGIN
                                CREATE TABLE weather(
                                id INT PRIMARY KEY,
                                main VARCHAR(40),
                                description VARCHAR(40),
                                icon CHAR(10),
                                valid_from datetime,
                                update_at datetime)
                            END;
                    """
constraint_fact_loc = """
                    IF OBJECT_ID('dbo.[FK_fact_location]') IS NULL
                        ALTER TABLE fact
                        ADD CONSTRAINT FK_fact_location
                        FOREIGN KEY (city_id) REFERENCES location(id);
                    """
constraint_fact_weather = """
                    IF OBJECT_ID('dbo.[FK_fact_weather]') IS NULL
                        ALTER TABLE fact
                        ADD CONSTRAINT FK_fact_weather
                        FOREIGN KEY (weather_id) REFERENCES weather(id);"""
def check_exist_DBS(cursor):
    cursor.execute("""
                        IF DB_ID('weatherDB') IS NOT NULL
                        BEGIN
                            PRINT 'DBS EXISTS'
                        END
                        ELSE
                        BEGIN
                            CREATE DATABASE weatherDB;
                            PRINT 'CREATE DBS';
                        END
                        """)

def create_table(cursor,drop_table = False):
    if drop_table == True:
        cursor.execute('drop table fact')
        cursor.execute('drop table location;')
        cursor.execute('drop table weather')

    cursor.execute(create_location)
    cursor.execute(create_weather)
    cursor.execute(create_fact_table)
    cursor.execute(constraint_fact_loc)
    cursor.execute(constraint_fact_weather)
