from flatten_json import flatten
import json
import crawl
import pandas as pd
import datetime
import crawl
import load 
import transform
import pyodbc
import create
import time
import json

#def crawling_schedule()

APIKEY = "9aaa6cdb9fmsh8cb6c6894c78dc9p16356fjsnc96b9e854eb3"
def ETL_process(cursor,location,API_KEY = APIKEY):
    nested_json = json.loads(crawl.get_request(api_key = APIKEY,city = location))
    flatten_json = flatten(nested_json)

    location = transform.load_location(flatten_json)
    fact = transform.load_fact(flatten_json)
    weather = transform.load_weather(nested_json)
    fact_merge = pd.concat([weather.rename(columns = {'id':'weather_id'})['weather_id'],fact],axis = 1).fillna(method = 'ffill')
    fact_merge = pd.concat([location.rename(columns ={'id':'city_id'})['city_id'],fact_merge],axis = 1).fillna(method = 'ffill')

    load.insert(cursor,location,'location')
    load.update(cursor,location,'location')

    load.insert(cursor,weather,'weather')
    load.update(cursor,weather,'weather')

    load.insert(cursor,fact_merge,'fact')
    
    print('ETL at {} completed'.format(transform.converttime(time.time())))

if __name__ == '__main__':
    API = "9aaa6cdb9fmsh8cb6c6894c78dc9p16356fjsnc96b9e854eb3"
    print('Check connection')
    conn = pyodbc.connect('DRIVER={SQL Server};'

                        'SERVER=CVPCHIPTK2-16\CHIPTK2;'

                        'DATABASE=master;'

                        'Trusted_Connection=yes;')
    conn.autocommit = True
    cursor = conn.cursor()
    create.check_exist_DBS(cursor)
    cursor.execute("USE weatherDB;")
    create.create_table(cursor)

    #Crawling data
    print('starting ETL')
    #Runing Schedule Crawling
    max_timer = 1800 #unit second
    city = str(input('Please add the city and country: '))
    run_time = 0
    while run_time <= max_timer:
        ETL_process(cursor,city,API)
        time.sleep(180)
        run_time += 180
    
    conn.execute("""BACKUP DATABASE weatherDB
                    TO DISK = 'E:\\ETL_homework\\backup\\WEATHER_DB.bak'; """)
    conn.close()





