import numpy as np
import pandas as pd
import time 
import transform


def update(cursor,df,df_name):
    if df_name == 'location':
        #in 13
        query = """
            IF EXISTS(SELECT id FROM location WHERE id = ?)
                IF NOT EXISTS(  SELECT id FROM location 
                                WHERE id = ? and name =? and coord_lon = ? and coord_lat = ? and timezone = ? and sys_country = ?)
                    UPDATE location 
                    SET name = ? , coord_lon = ? , coord_lat = ? , timezone = ? ,sys_country = ?,update_at =?
                    WHERE id = ?
                    PRINT 'UPDATED'
        """
    elif df_name == 'weather':
        #in = 9

        query = """
            IF EXISTS(SELECT id FROM weather WHERE id = ?)
                IF NOT EXISTS(  SELECT id FROM weather 
                                WHERE id = ? and main =? and description = ? and icon = ?)
                    UPDATE weather
                    SET main =?, description = ? , icon = ?, update_at = ?
                    WHERE id = ?
                    PRINT 'UPDATED';

        """
    for row in range(len(df)):
        cursor.execute(query,[convert_value(df.iloc[row,:].values)[0]] +convert_value(df.iloc[row,:-2].values)
                    + convert_value(df.iloc[row,:].values)[1:-2]+ [transform.converttime(time.time())] + [convert_value(df.iloc[row,:].values)[0]])

def insert_auto_generate_fea_val(df, df_name):
    """ 
        automatic generate query for insert data
    """
    features = '{}'.format(tuple(df.columns)).replace('\'','')
    values = '{}'.format(tuple(['?']*len(df.columns)))
    query = 'INSERT INTO {} {} VALUES {}'.format(df_name,features,values.replace('\'','')) 
    return query
    
import math
# def convert_value(list_values):
#     """
#         convert type np.float64/int64 into raw type of python for the requirement of SQL query
#     """
#     temp = []
#     for i in list_values:
#         if type(i) == np.int64 or type(i) == np.float64:
#             i = i.item()
#             if math.isnan(i):
#                 i = None
#         temp.append(i)
#     return temp

def convert_value(dict_list_values):
    """
        convert type np.float64/int64 into raw type of python for the requirement of SQL query
    """
    temp = []
    for i in list_values:
        if type(i) == np.int64 or type(i) == np.float64:
            i = i.item()
            if math.isnan(i):
                i = None
        temp.append(i)
    return temp

def insert(cursor,df,df_name):
    """
        insert values into existing table, df is the dataframe extracted from api response ans load through pandas dataframe
    """
    if df_name == 'location' or df_name == 'weather':
        for row in range(len(df)):
            cursor.execute("IF NOT EXISTS(SELECT id FROM {} WHERE id = ?) ".format(df_name) + insert_auto_generate_fea_val(df,df_name) +"; PRINT('INSERTED');",
            [convert_value(df.iloc[row].values)[0]] + convert_value(df.iloc[row].values))
            #print('Insert into {} succesfully'.format(df_name))
    elif df_name =='fact':
        for row in range(len(df)):
            cursor.execute("IF NOT EXISTS(SELECT dt,weather_id,city_id FROM fact WHERE dt = ? and weather_id = ? and city_id = ?) ".format(df_name) + insert_auto_generate_fea_val(df,df_name) + "; PRINT('INSERTED');",
                [df.loc[row,'dt']] + convert_value(df.loc[row,['weather_id','city_id']].values) + convert_value(df.iloc[row].values))
            #print('Insert into {} succesfully'.format(df_name))
