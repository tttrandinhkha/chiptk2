from xmlrpc.client import ResponseError
import requests
import numpy as np
import json

URL = "https://community-open-weather-map.p.rapidapi.com/weather"
api_key = "9aaa6cdb9fmsh8cb6c6894c78dc9p16356fjsnc96b9e854eb3"
# Check response status
def get_request(api_key= api_key,url=URL ,city='London,uk'):
	"""create custom request, this function takes in only city, next improvement is in the undefined future

	Args:
		city (str, optional): _city and abbreviation of country_. Defaults to 'London,uk'.
		units (str, optional): _unit of temperature_. Defaults to 'imperial'.
		mode (str, optional): Default to 'json'
	"""
	querystring = {"q":city}

	headers = {"X-RapidAPI-Host": "community-open-weather-map.p.rapidapi.com","X-RapidAPI-Key": api_key}
	response = requests.request("GET", url=URL, headers=headers, params=querystring)
	#Check request status
	return response.status_code
		