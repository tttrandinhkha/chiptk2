# Unit Tests ASSIGNMENT

## Description:
This assignment using pytest to testing ETL process

## Project structure:
* src folder contains multiple py file for the last ETL assignment
* tests folder contains test.py for running unit test
* test.py actions: check success response, check transform step: testing location input for crawling, check_range, convert_time, load_location, load_weather 

## Installation:
This assignment only running unit test in Python version = 3.7.5

## Setup and process:
Before running, please install necessary libraries in file requirement.txt.
Open terminal, running coverage 
1. change directory into tests folder
2. run -m pytest test.py
3. coverage report -m
