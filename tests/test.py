import src.transform as transform
import src.crawl as crawl
import pytest 

@pytest.mark.parametrize('location,expected_value',
                        [
                            ('Ho Chi Minh,VN',200),
                            ('ACB',404)
                        ],)
def test_location_input(location,expected_value):
    """ Check valid locaiton input
    """
    assert crawl.get_request(city = location) == expected_value

@pytest.mark.parametrize(
                        "date_obj,expected_value",
                        [
                            (1654036201,'2022-05-31 22:30:01'),
                            (1654036201.773475,'2022-05-31 22:30:01'),
                            ('abc',None)
                        ],
)
def test_datetime(date_obj,expected_value):
    """Test convert value into type datetime"""
    assert transform.convert_time(date_obj) == expected_value

@pytest.mark.parametrize(
                        "x,min,max,expected_value",
                        [
                            (2.5,0,4,2.5),
                            (7,4,10,7),
                            (-20,-35,43,-20),
                            (0,1,3,None),
                            ('-20',-35,43,None),
                        ],
)
def test_range_value(x,min,max,expected_value):
    """Test valid range for value in fact table"""
    assert transform.check_range(x,min,max) == expected_value

@pytest.mark.parametrize("flatten_json,expected_list",
                        [
                           ({   'coord_lon': 106.6667,
                                'coord_lat': 10.75,
                                'weather_0_id': 502,
                                'weather_0_main': 'Rain',
                                'weather_0_description': 'heavy intensity rain',
                                'weather_0_icon': '10d',
                                'base': 'stations',
                                'main_temp': 302.16,
                                'main_feels_like': 308.82,
                                'main_temp_min': 302.16,
                                'main_temp_max': 302.16,
                                'main_pressure': 1008,
                                'main_humidity': 84,
                                'visibility': 10000,
                                'wind_speed': 1.54,
                                'wind_deg': 0,
                                'rain_1h': 4.86,
                                'clouds_all': 75,
                                'dt': 1654053384,
                                'sys_type': 1,
                                'sys_id': 9314,
                                'sys_country': 'VN',
                                'sys_sunrise': 1654036201,
                                'sys_sunset': 1654081952,
                                'timezone': 25200,
                                'id': 1566083,
                                'name': 'Ho Chi Minh City',
                                'cod': 200}, [1566083,'Ho Chi Minh City',106.6667,10.75,25200,'VN']),
                                ({   'coord_lon': 106.6667,
                                'coord_lat': 10.75,
                                'weather_0_id': 502,
                                'weather_0_main': 'Rain',
                                'weather_0_description': 'heavy intensity rain',
                                'weather_0_icon': '10d',
                                'base': 'stations',
                                'main_temp': 302.16,
                                'main_feels_like': 308.82,
                                'main_temp_min': 302.16,
                                'main_temp_max': 302.16,
                                'main_pressure': 1008,
                                'main_humidity': 84,
                                'visibility': 10000,
                                'wind_speed': 1.54,
                                'wind_deg': 0,
                                'rain_1h': 4.86,
                                'clouds_all': 75,
                                'dt': 1654053384,
                                'sys_type': 1,
                                'sys_id': 9314,
                                'sys_country': 'VN',
                                'sys_sunrise': 1654036201,
                                'sys_sunset': 1654081952,
                                'timezone': 25200,
                                'name': 'Ho Chi Minh City',
                                'cod': 200}, None),
                        ],)
def test_location(flatten_json,expected_list):
    """Test valid location id"""
    assert transform.load_location(flatten_json,return_list=True) == expected_list


@pytest.mark.parametrize("nested_json,expected_list",
                        [
                           ({   'coord': {'lon': 106.6667, 'lat': 10.75},
                                'weather': [{'id': 502,
                                            'main': 'Rain',
                                            'description': 'heavy intensity rain',
                                            'icon': '10d'}],
                                'base': 'stations',
                                'main': {'temp': 302.16,
                                'feels_like': 308.82,
                                'temp_min': 302.16,
                                'temp_max': 302.16,
                                'pressure': 1008,
                                'humidity': 84},
                                'visibility': 10000,
                                'wind': {'speed': 1.54, 'deg': 0},
                                'rain': {'1h': 4.86},
                                'clouds': {'all': 75},
                                'dt': 1654053384,
                                'sys': {'type': 1,
                                'id': 9314,
                                'country': 'VN',
                                'sunrise': 1654036201,
                                'sunset': 1654081952},
                                'timezone': 25200,
                                'id': 1566083,
                                'name': 'Ho Chi Minh City',
                                'cod': 200}
                                ,[502,'Rain','heavy intensity rain','10d']),
                                ({'coord': {'lon': 106.6667, 'lat': 10.75},
                                'weather': [{'id': '502',
                                'main': 'Rain',
                                'description': 'heavy intensity rain',
                                'icon': '10d'}],
                                'base': 'stations',
                                'main': {'temp': 302.16,
                                'feels_like': 308.82,
                                'temp_min': 302.16,
                                'temp_max': 302.16,
                                'pressure': 1008,
                                'humidity': 84},
                                'visibility': 10000,
                                'wind': {'speed': 1.54, 'deg': 0},
                                'rain': {'1h': 4.86},
                                'clouds': {'all': 75},
                                'dt': 1654053384,
                                'sys': {'type': 1,
                                'id': '9314',
                                'country': 'VN',
                                'sunrise': 1654036201,
                                'sunset': 1654081952},
                                'timezone': 25200,
                                'id': 1566083,
                                'name': 'Ho Chi Minh City',
                                'cod': 200}
                                , None),
                                ],)
def test_weather(nested_json,expected_list):
    "Test valid weather id"
    assert transform.load_weather(nested_json,return_list=True) == expected_list